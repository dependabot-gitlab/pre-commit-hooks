# Pre-Commit Hooks

Pre-Commit hooks used by [dependabot-gitlab](https://gitlab.com/dependabot-gitlab/dependabot) project

# Usage

Install and configure [pre-commit](https://pre-commit.com/#usage)

Hooks are using via bundler and require `rspec`, `reek` and `rubocop` to be installed
